# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 10:12:21 2020

@author: hongsong chou
"""

import time
import os
import pandas as pd
from datetime import timedelta
from common.OrderBookSnapshot_FiveLevels import OrderBookSnapshot_FiveLevels
import sys
#print(os.listdir('../'))
sys.path.append('../processedData/')

class MarketDataService:

    def __init__(self, marketData_2_exchSim_q, marketData_2_platform_q):
        print(sys.path)
        print('loading data......')
        CDFD9 = pd.read_csv(
            '/Users/felixfeng/PycharmProjects/quizez/project/processedData/TMX/2019/201904/2019-04-01/CDFD9.csv',
            #'processedData/TMX/2019/201904/20190401/CDFD9.csv',
            index_col=0)
        stock2330 = pd.read_csv(
            '/Users/felixfeng/PycharmProjects/quizez/project/processedData/TSE/2019/201904/2019-04-01/2330.csv.gz',
            index_col=0)
        print('processing data......')
        stock2330.drop(['lastPx', 'size', 'volume', 'filledFlag', 'stopMatch', 'testMatch'], axis=1, inplace=True)
        stock2330['ticker'] = 'stock2330'
        CDFD9.drop(['deriveBuyPrice', 'deriveSellPrice', 'deriveBuyQty', 'deriveSellQty', 'deriveFlag'], axis=1,
                   inplace=True)
        CDFD9['ticker'] = 'futureCDFD9'
        dt = pd.concat([stock2330, CDFD9], axis=0).sort_values(by='time')
        dt.reset_index(drop=True, inplace=True)
        dt = dt.loc[dt['time'] >= 90000000]
        dt.reset_index(drop=True, inplace=True)
        dt['time'] = pd.to_datetime('20190401' + dt['time'].astype('str'), format='%Y%m%d%H%M%S%f')
        dt['interval'] = dt['time'].diff(1)
        dt['interval'] = dt['interval'].shift(-1)
        dt['interval'].fillna(timedelta(seconds=0), inplace=True)
        dt['time'] = dt['time'].dt.time
        #print(dt)
        print("[%d]<<<<< call MarketDataService.init" % (os.getpid(),))
        time.sleep(3)
        self.produce_market_data(marketData_2_exchSim_q, marketData_2_platform_q, dt)

    def produce_market_data(self, marketData_2_exchSim_q, marketData_2_platform_q, dt):
        for i in range(dt.shape[0]):
            di = dt.loc[i]
            self.produce_quote(marketData_2_exchSim_q, marketData_2_platform_q, di)
            time.sleep(di.interval.total_seconds())

    def produce_quote(self, marketData_2_exchSim_q, marketData_2_platform_q, di):
        bidPrice = [di.bidPrice1, di.bidPrice2, di.bidPrice3, di.bidPrice4, di.bidPrice5]
        askPrice = [di.askPrice1, di.askPrice2, di.askPrice3, di.askPrice4, di.askPrice5]
        bidSize = [di.bidSize1, di.bidSize2, di.bidSize3, di.bidSize4, di.bidSize5]
        askSize = [di.askSize1, di.askSize2, di.askSize3, di.askSize4, di.askSize5]

        quoteSnapshot = OrderBookSnapshot_FiveLevels(di.ticker, '20190401', di.time,
                                                     bidPrice, askPrice, bidSize, askSize)
        #print('[%d]MarketDataService>>>produce_quote' % (os.getpid()))
        #print(quoteSnapshot.outputAsDataFrame())
        marketData_2_exchSim_q.put(quoteSnapshot)
        marketData_2_platform_q.put(quoteSnapshot)