# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 10:12:21 2020

@author: hongsong chou
"""

import threading
import os
import time
from common.SingleStockExecution import SingleStockExecution
from multiprocessing import Queue
from multiprocessing import Manager


class buyOrderQueue:
    def __init__(self):
        self.q = []

    def put(self, order):
        self.q.append(order)

    def get_order(self):
        self.q = sorted(self.q, key=lambda x:x.price, reverse=True)
        if self.q:
            res = self.q[0]
            self.q.remove(res)
            return res
        else:
            return None


class sellOrderQueue:
    def __init__(self):
        self.q = []

    def put(self, order):
        self.q.append(order)

    def get_order(self):
        self.q = sorted(self.q, key = lambda x: x.price)
        #print('after sorting')
        #print(self.q)
        if len(self.q) > 0:
            res = self.q[0]
            self.q.remove(res)
            #print(res)
            return res
        return None


class ExchangeSimulator:
    def __init__(self, marketData_2_exchSim_q, platform_2_exchSim_order_q, exchSim_2_platform_execution_q):
        print("[%d]<<<<< call ExchSim.init" % (os.getpid(),))

        self.orderbook = {}
        self.BuyOrderQueue = buyOrderQueue()
        self.SellOrderQueue = sellOrderQueue()

        t_md = threading.Thread(name='exchsim.on_md', target=self.consume_md, args=(marketData_2_exchSim_q,))
        t_md.start()
        
        t_order = threading.Thread(name='exchsim.on_order', target=self.consume_order, args=(platform_2_exchSim_order_q, exchSim_2_platform_execution_q, ))
        t_order.start()

    def consume_md(self, marketData_2_exchSim_q):
        while True:
            res = marketData_2_exchSim_q.get()
            print('[%d]ExchSim.consume_md' % (os.getpid()))
            #print(res.outputAsDataFrame().loc[0])
            self.orderbook[res.outputAsDataFrame().loc[0]['ticker']] = res.outputAsDataFrame().loc[0]['ticker']
    
    def consume_order(self, platform_2_exchSim_order_q, exchSim_2_platform_execution_q):
        while True:
            res = platform_2_exchSim_order_q.get()
            print("Getting an order")
            #print(res.outputAsArray())
            if res.direction == 'buy' and res.type == 'LO':
                self.BuyOrderQueue.put(res)
                res_buy = self.BuyOrderQueue.get_order()
                self.produce_execution(res_buy, exchSim_2_platform_execution_q)
            elif res.direction == 'buy' and res.type == 'LO':
                self.SellOrderQueue.put(res)
                res_sell = self.SellOrderQueue.get_order()
                self.produce_execution(res_sell, exchSim_2_platform_execution_q)
            elif res.type == 'MO':
                self.produce_execution(res, exchSim_2_platform_execution_q)
            """
            print("Order in queue")
            res_buy = self.BuyOrderQueue.get_order()
            res_sell = self.SellOrderQueue.get_order()
            print(res_buy)
            print(res_sell)

            if res_buy is not None:
                #print(res_buy.outputAsArray())
                self.produce_execution(res_buy, exchSim_2_platform_execution_q)
            if res_sell is not None:
                #print(res_sell.outputAsArray())
                self.produce_execution(res_sell, exchSim_2_platform_execution_q)
            """

    def produce_execution(self, order, exchSim_2_platform_execution_q):
        execution = SingleStockExecution(order.ticker, order.date, time.asctime(time.localtime(time.time())))
        execution.direction = order.direction
        if order.type == 'MO':
            i = 1
            if order.direction == 'buy':
                while i < 6:
                    execution.price = self.orderbook[order.ticker]['askPrice{}'.format(i)]
                    execution.size = min(self.orderbook[order.ticker]['askSize{}'.format(i)], order.size)
                    exchSim_2_platform_execution_q.put(execution)
                    order.size = order.size - execution.size
                    i += 1
                    if i == 5 and order.size > 0:
                        order.price = self.orderbook[order.ticker]['askPrice5']
                        platform_2_exchSim_order_q.put(order)

            elif order.direction == 'sell':
                while i < 6:
                    execution.price = self.orderbook[order.ticker]['bidPrice{}'.format(i)]
                    execution.size = min(self.orderbook[order.ticker]['bidSize{}'.format(i)], order.size)
                    exchSim_2_platform_execution_q.put(execution)
                    order.size = order.size - execution.size
                    i += 1
                    if i == 5 and order.size > 0:
                        order.price = self.orderbook[order.ticker]['bidPrice5']
                        platform_2_exchSim_order_q.put(order)

        elif order.type == 'LO':
            if order.direction == 'buy':
                pass
            elif order.direction == 'sell':
                pass
